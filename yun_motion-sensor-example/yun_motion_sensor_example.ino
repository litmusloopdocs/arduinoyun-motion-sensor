/*
 Litmus Loop
*/

/*
Inlcude all the library files 
*/
#include <loopmq.h>
#include <Bridge.h>
#include<BridgeClient.h>
#include <stdlib.h>
#include <stdio.h>
#include <ArduinoJson.h> 
#include "configuration.h"

/*
Passing the defined information to char
*/
int port = port_number;
char hostname[] = server;                     // Server name
char c[15] = clientID;                        // ClientID
char pass[16]= password;                      // password
char user[16] = userID;                       // username
char p[]= subTOPIC;                           // Subscribe on this topic to get the data
char s[]= pubTopic;                           // Publish on this topic to send data or command to device 

/*macro definitions of PIR motion sensor pin and LED pin*/
#define PIR_MOTION_SENSOR 2//Use pin 2 to receive the signal from the module
#define LED 4//the Grove - LED is connected to D4 of Arduino

BridgeClient yunClient;                       // yun client
PubSubClient loopmq(yunClient);               // instance to use the loopmq functions.

void callback(char* topic, byte* payload, unsigned int length) {
 Serial.print("Message arrived [");
 Serial.print(topic);
 Serial.print("] ");
} 

void reconnect() {                        
   
  while (!loopmq.connected()) {               // Loop until we're reconnected
  Serial.print("Attempting MQTT connection...");    // Attempt to connect    
    if (loopmq.connect(c,user,pass)) {
    Serial.println("connected");            // Once connected, publish an announcement...
         
         loopmq.subscribe(s);                 // Subscribe to a topic  
/*  
To Unscubscribe from a Topic uncomment the code below   
*/
        
   // loopmq.unsubscribe(s); 
   

      /*
To disconnect unocmment the code below
*/
      
   //  loopmq.disconnect();            
    } 
    else {
   Serial.print("failed, rc=");
   Serial.print(loopmq.state());
   Serial.println(" try again in 5 seconds");
      
      delay(5000);                            // Wait 5 seconds before retrying
    }
  }
}

void setup()
{
   /* 
   Bridge takes about two seconds to start up
   it can be helpful to use the on-board LED
   as an indicator for when it has initialized
  */
  
  Bridge.begin();   
  Serial.begin(57600);                //Start the Serial Connection
   
  pinMode(PIR_MOTION_SENSOR, INPUT);
  pinMode(LED,OUTPUT);
  
  loopmq.setServer(hostname, port);           // Connect to the specified server and port defined by the user
  loopmq.setCallback(callback);               // Call the callbeck funciton when published    

  delay(1500); 
}

void loop()
{
  /*
  Sensor Code for PIR motion sensor
  */
    bool sensorValue = digitalRead(PIR_MOTION_SENSOR);
    if(sensorValue == HIGH)//if the sensor value is HIGH?
    { 
        digitalWrite(LED, HIGH); 
        //Serial.print(sensorValue);
    }
    else
    {
        //Serial.print(sensorValue);
        digitalWrite(LED, LOW);        
    }
/*  
  JSON parser
*/
  StaticJsonBuffer<200> jsonBuffer;               //  Inside the brackets, 200 is the size of the pool in bytes.If the JSON object is more complex, you need to increase that value. 

  JsonObject& root = jsonBuffer.createObject();   // It's a reference to the JsonObject, the actual bytes are inside the JsonBuffer with all the other nodes of the object tree.
  root["command"] = "PIR motion sensor";                     // Add values in the object, add the objects you want to add to the JSON in the form of root["key'] = value;

  JsonObject& data= root.createNestedObject("data"); // nested JSON 
  data["Motion value"]= sensorValue;                                // Add data["key"]= value


  root.printTo(Serial);                           // prints to serial terminal
  Serial.println();

  char buffer[100];                               // buffer to pass as payload
  root.printTo(buffer, sizeof(buffer));           // copy the JSON to the buffer to pass as a payload 
  /*
  Publish to the server
  */
  
  if (!loopmq.connected()) {
    reconnect();                              // Try to reconnect if connection dropped
  }
  if (loopmq.connect(c, user, pass)){
 
   loopmq.publish(p,buffer);                     // Publish message to the server once only
     delay(1000);
  }
  
  loopmq.loop();                              // check if the network is connected and also if the client is up and running
 
}

